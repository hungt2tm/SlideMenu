//
//  InfoViewController.swift
//  iPageControl
//
//  Created by Đỗ Hưng on 19/11/2021.
//

import UIKit

class InfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "About This App"
        view.backgroundColor = .systemGreen
    }
}
