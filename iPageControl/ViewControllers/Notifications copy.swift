//
//  Notifications.swift
//  AkiPan
//
//  Created by Hint on 5/9/19.
//  Copyright © 2019 MasSSD. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

private let noticesVi = [
    "Mau mở app ngay, không bay hết từ. Mở ngay MochiMochi để \"giữ lại\" từ vựng của bạn nào.",
    "Đừng xanh như lá, đừng lười như sâu. Từ vựng không học lâu lâu sẽ quên hết đó T_T.",
    "Học đi, học đi, học rồi sẽ giỏi cùng MochiMochi ",
    "Bạn có nhớ Mochi không?",
    "Rảnh tay phải ôn tập ngay. Mở MochiMochi ôn tập ngay nào.",
    "Đến giờ học từ rồi, mở app thôi.",
    "Mở app nhanh thôi, không trôi từ vựng.",
    "Tuần mới đến rồi, học từ vựng thôi.",
    "Học mà không chơi đánh rơi tuổi trẻ, chơi mà không học bán rẻ tương lai. Mở MochiMochi học ngay nào ^^",
    "Tương lai khóc hay cười phụ thuộc vào độ lười của bạn.",
    "Nắng vàng trên những tán cây, từ mới đã có ở ngay MochiMochi đây nè!",
    "Mochi như một ly trà, \"học\" ngay cho nóng mới là \"dân chơi\"",
    "Mở app Mochi, luyện thi ngay chứ nhỉ?",
    "Đi mô làm chi, đừng quên MochiMochi. Học từ vựng ngay thôi nào!",
    "Bạn đang rảnh tay? Hãy ôn tập ngay!",
    "Bạn ơi sắp rơi từ vựng rồi nè! Vô ngay MochiMochi lượm từ vựng nào!",
    "Học cùng Mochi, không giỏi hơi phí. Mở ngay app học để trở thành cao thủ từ vựng nào!",
    "Nắng đã có mũ, mưa đã có ô, muốn giỏi tiếng Anh đã có MochiMochi. Mở app học ngay thôi nào.",
    "Cốc cốc cốc, Mochi mang từ mới đến cho bạn nè. Mở app nhận từ nào.",
    "Mở MochiMochi là cười hihi. Cùng cười và học từ vựng nào ^^"
]

private let noticesEng = [
    "Practice makes perfect. Practice now.",
    "Take a rest and review your words now.",
    "It's time to learn Japanese with MochiMochi.",
    "My dear, MochiMochi miss you.",
    "MochiMochi send you some new words. Open app and receive now.",
    "Japanese will bring you to the next chapter of your life. Learn Japanese with MochiMochi now.",
    "Enjoy Japanese with Mochimochi now. Why not?"
]

let noticeReviewTime = "NoticeReviewTime"
let noticeNineHourPM = "NoticeNineHourPM"
let noticeReviewList = "NoticeReviewList"

class Notifications {
    
    static var shared  = Notifications()
    var isRequest = false
    
    private func userRequest() {
//        let newNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge , .sound], categories: nil)
//        UIApplication.shared.registerUserNotificationSettings(newNotificationSettings)
        appDelegate.configureNotification()
        isRequest = true
        UserDefaults.standard.set(true, forKey: UserDefaultKeys.NOTICE_IS_REQUEST)
        UserDefaults.standard.synchronize()
        
        appDelegate.fireBaseLogEvent(eventName: "notice_accepted_account",
        itemID: UIDevice.current.identifierForVendor?.uuidString,
        itemName: UIDevice.current.identifierForVendor?.uuidString,
        contentType: "notice_accepted_account")
    }
    
    func scheduleNotification(title: String, body: String, date: Date, review_count: Int) {
        if !isRequest { self.userRequest() }
        let notification = UILocalNotification()
        notification.category = noticeReviewTime
        notification.fireDate = date
        notification.alertTitle = title
        notification.alertBody = body
        let max = 9999
        var badge = review_count
        if badge > max {
            badge = max
        }
        notification.applicationIconBadgeNumber = badge
//        UIApplication.shared.cancelAllLocalNotifications()
        for notification in UIApplication.shared.scheduledLocalNotifications! {
            if notification.category == noticeReviewTime || notification.category == noticeNineHourPM {
                UIApplication.shared.cancelLocalNotification(notification)
            }
        }
        UIApplication.shared.scheduleLocalNotification(notification)
        self.scheduleNotificationNineHour()
    }
    
    func register() {
        if !isRequest { self.userRequest() }
        self.scheduleNotificationNineHour()
    }
    
    func scheduleNotificationCurrent(title: String, body: String, date: Date) {
        let notification = UILocalNotification()
        notification.category = noticeReviewList + date.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ")
        notification.fireDate = date
        notification.alertTitle = title
        notification.alertBody = body
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func scheduleNotificationNineHour() {
        var date = DateComponents()
        date.hour = 21
        /*Test
        let d = Date()
        let calendar = Calendar.current
        date.year = calendar.component(.year, from: d)
        date.month = calendar.component(.month, from: d)
        date.day = calendar.component(.day, from: d)
        date.hour = calendar.component(.hour, from: d)
        date.minute = calendar.component(.minute, from: d)
        date.second = calendar.component(.second, from: d) + 1
        End Test*/
        if let fireDate = Calendar.current.date(from: date) {
            let notification = UILocalNotification()
            notification.fireDate = fireDate
            notification.category = noticeNineHourPM
            notification.alertTitle = LanguageStrings.NOTICE_NINE_PM_TITLE.localized
            let message = global.language == .vi ? noticesVi.randomElement() : noticesEng.randomElement()
            notification.alertBody = message ?? LanguageStrings.NOTICE_NINE_PM_MESSAGE.localized
            notification.repeatInterval = .day
            if let notifs = UIApplication.shared.scheduledLocalNotifications {
                for notif in notifs {
                    if notif.category == noticeNineHourPM {
                        UIApplication.shared.cancelLocalNotification(notif)
                        break
                    }
                }
            }
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
}

@available(iOS 10.0, *)
class NotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    static var shared  = NotificationDelegate()
    var isRequest = false
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func update() {
        if NotificationDelegate.shared.notificationCenter.delegate == nil {
            NotificationDelegate.shared.notificationCenter.delegate = self
        }
    }
    
    private func userRequest() {
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            self.isRequest = true
            UserDefaults.standard.set(didAllow, forKey: UserDefaultKeys.NOTICE_IS_REQUEST)
            UserDefaults.standard.synchronize()
            if !didAllow {
                print("User has declined notifications")
            }else {
                appDelegate.fireBaseLogEvent(eventName: "notice_accepted_account",
                                             itemID: UIDevice.current.identifierForVendor?.uuidString,
                                             itemName: UIDevice.current.identifierForVendor?.uuidString,
                                             contentType: "notice_accepted_account")
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    func scheduleNotification(title: String, body: String, date: Date, review_count: Int) {
        if !isRequest { self.userRequest() }
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        let max = 9999
        var badge = review_count
        if badge > max {
             badge = max
        }
        content.badge = NSNumber(value: badge)
        content.sound = UNNotificationSound.default
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        let request = UNNotificationRequest(identifier: noticeReviewTime, content: content, trigger: trigger)
        notificationCenter.add(request)
        self.scheduleNotificationNineHour()
    }
    
    func register() {
        if !isRequest { self.userRequest() }
        self.scheduleNotificationNineHour()
    }
    
    
    func scheduleNotificationCurrent(title: String, body: String, date: Date) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        let identifier = noticeReviewList + date.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ")
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        notificationCenter.add(request)
    }
    
    func scheduleNotificationNineHour() {
        let content = UNMutableNotificationContent()
        content.title = LanguageStrings.NOTICE_NINE_PM_TITLE.localized
        let message = global.language == .vi ? noticesVi.randomElement() : noticesEng.randomElement()
        content.body = message ?? LanguageStrings.NOTICE_NINE_PM_MESSAGE.localized
        content.sound = UNNotificationSound.default
        var dateMatching = DateComponents()
        dateMatching.hour = 21
        /* Test
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        dateMatching.hour = hour
        dateMatching.minute = minutes + 1
        End Test*/
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateMatching, repeats: false)
        let request = UNNotificationRequest(identifier: noticeNineHourPM, content: content, trigger: trigger)
        notificationCenter.add(request)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let identifier = notification.request.identifier
        if identifier == noticeNineHourPM {
            scheduleNotificationNineHour()
        } else if identifier == noticeReviewTime || identifier == noticeReviewList {
            NotificationCenter.default.post(name: .updateNextReviewCount, object: nil)
        }
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let identifier = response.notification.request.identifier
        if identifier == noticeReviewTime || identifier == noticeReviewList {
            print("Handling Local Notification - Review Time")
            appDelegate.fireBaseLogEvent(eventName: "click_perfect_time_notice",
            itemID: UIDevice.current.identifierForVendor?.uuidString,
            itemName: UIDevice.current.identifierForVendor?.uuidString,
            contentType: "click_perfect_time_notice")
        } else {
            appDelegate.fireBaseLogEvent(eventName: "click_other_notice",
            itemID: UIDevice.current.identifierForVendor?.uuidString,
            itemName: UIDevice.current.identifierForVendor?.uuidString,
            contentType: "click_other_notice")
        }
        completionHandler()
    }
}
