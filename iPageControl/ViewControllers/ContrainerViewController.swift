//
//  ContrainerViewController.swift
//  iPageControl
//
//  Created by Đỗ Hưng on 18/11/2021.
//

import UIKit

class ContrainerViewController: UIViewController {
    
    enum MenuState {
        case opened
        case closed
    }
    
    private var menuState: MenuState = .closed
    
    private let menuVC = MenuViewController()
    private let homeVC = HomeViewController()
    private var navi: UINavigationController?
    private lazy var infoVC = InfoViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        addChildViewControllers()
    }
    
    private func addChildViewControllers() {
        // Menu
        menuVC.delegate = self
        addChild(menuVC)
        view.addSubview(menuVC.view)
        menuVC.didMove(toParent: self)
        
        // Home
        homeVC.delegate = self
        let navi = UINavigationController(rootViewController: homeVC)
        addChild(navi)
        view.addSubview(navi.view)
        navi.didMove(toParent: self)
        self.navi = navi
    }
}


extension ContrainerViewController: HomeViewControllerDelegate {
    func didTapMenuButton() {
        // Animate Menu
        toggleMenu()
    }
    
    func toggleMenu(completion: (() -> Void)? = nil) {
        switch menuState {
        case .closed:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut) {
                self.navi?.view.frame.origin.x = self.homeVC.view.frame.self.width - 100
            } completion: { [weak self] done in
                if done {
                    self?.menuState = .opened
                }
            }
        case .opened:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut) {
                self.navi?.view.frame.origin.x = 0
            } completion: { [weak self] done in
                if done {
                    self?.menuState = .closed
                    DispatchQueue.main.async {
                        completion?()
                    }
                }
            }
        }
    }
}

extension ContrainerViewController: MenuViewControllerDelegate {
    func didSelect(menuItem: MenuViewController.MenuOptions) {
        toggleMenu(completion: nil)
        switch menuItem {
        case .home:
            self.resetToHome()
        case .info:
            self.addInfo()
        case .appRating:
            break
        case .shareApp:
            break
        case .settings:
            break
        }
    }
    
    func addInfo() {
        let vc = self.infoVC
        homeVC.addChild(vc)
        homeVC.view.addSubview(vc.view)
        vc.view.frame = view.frame
        vc.didMove(toParent: homeVC)
        homeVC.title = vc.title
    }
    
    func resetToHome() {
        infoVC.view.removeFromSuperview()
        infoVC.didMove(toParent: nil)
        homeVC.title = "Home"
    }
}
